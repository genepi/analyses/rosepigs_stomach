condition	line	feed	name	ID	sex	name_f	name_m	family	tissue	bande1	birth_weight	d_weaning	order	time	nfcore_id
HRFI_fasted_R1	HRFI	fasted	FR17MAG201912990	2990	F	FR17MAG201811418	FR17MAG201811893	A	stomach	GJS201910	1770	14AUG2019:00:00:00	3	09:18	HRFI_fasted_R1
HRFI_fasted_R2	HRFI	fasted	FR17MAG201912992	2992	M	FR17MAG201811418	FR17MAG201811893	A	stomach	GJS201910	1870	14AUG2019:00:00:00	14	10:47	HRFI_fasted_R2
HRFI_fasted_R3	HRFI	fasted	FR17MAG201913004	3004	F	FR17MAG201811206	FR17MAG201811884	B	stomach	GJS201910	1050	14AUG2019:00:00:00	13	10:30	HRFI_fasted_R3
HRFI_fasted_R4	HRFI	fasted	FR17MAG201913011	3011	M	FR17MAG201811206	FR17MAG201811884	B	stomach	GJS201910	1280	14AUG2019:00:00:00	12	10:28	HRFI_fasted_R4
HRFI_fasted_R5	HRFI	fasted	FR17MAG201913042	3042	F	FR17MAG201811508	FR17MAG201811894	C	stomach	GJS201910	1370	14AUG2019:00:00:00	2	09:10	HRFI_fasted_R5
HRFI_fasted_R6	HRFI	fasted	FR17MAG201913050	3050	M	FR17MAG201811508	FR17MAG201811894	C	stomach	GJS201910	1200	14AUG2019:00:00:00	9	10:04	HRFI_fasted_R6
HRFI_fed_R1	HRFI	fed	FR17MAG201912989	2989	F	FR17MAG201811418	FR17MAG201811893	A	stomach	GJS201910	1830	14AUG2019:00:00:00	16	10:57	HRFI_fed_R1
HRFI_fed_R2	HRFI	fed	FR17MAG201912991	2991	M	FR17MAG201811418	FR17MAG201811893	A	stomach	GJS201910	1710	14AUG2019:00:00:00	18	11:07	HRFI_fed_R2
HRFI_fed_R3	HRFI	fed	FR17MAG201913012	3012	M	FR17MAG201811206	FR17MAG201811884	B	stomach	GJS201910	1790	14AUG2019:00:00:00	11	10:26	HRFI_fed_R3
HRFI_fed_R4	HRFI	fed	FR17MAG201913014	3014	M	FR17MAG201811206	FR17MAG201811884	B	stomach	GJS201910	1640	14AUG2019:00:00:00	24	11:46	HRFI_fed_R4
HRFI_fed_R5	HRFI	fed	FR17MAG201913039	3039	F	FR17MAG201811508	FR17MAG201811894	C	stomach	GJS201910	1430	14AUG2019:00:00:00	21	11:25	HRFI_fed_R5
HRFI_fed_R6	HRFI	fed	FR17MAG201913049	3049	M	FR17MAG201811508	FR17MAG201811894	C	stomach	GJS201910	1580	14AUG2019:00:00:00	15	10:52	HRFI_fed_R6
LRFI_fasted_R1	LRFI	fasted	FR17MAG201912884	2884	F	FR17MAG201811642	FR17MAG201811513	D	stomach	GJS201910	1040	14AUG2019:00:00:00	6	09:43	LRFI_fasted_R1
LRFI_fasted_R2	LRFI	fasted	FR17MAG201912889	2889	M	FR17MAG201811642	FR17MAG201811513	D	stomach	GJS201910	1450	14AUG2019:00:00:00	8	10:00	LRFI_fasted_R2
LRFI_fasted_R3	LRFI	fasted	FR17MAG201912899	2899	F	FR17MAG201811822	FR17MAG201812088	E	stomach	GJS201910	1200	14AUG2019:00:00:00	5	09:34	LRFI_fasted_R3
LRFI_fasted_R4	LRFI	fasted	FR17MAG201912902	2902	M	FR17MAG201811822	FR17MAG201812088	E	stomach	GJS201910	1530	14AUG2019:00:00:00	4	09:30	LRFI_fasted_R4
LRFI_fasted_R5	LRFI	fasted	FR17MAG201912980	2980	F	FR17MAG201812017	FR17MAG201811844	F	stomach	GJS201910	1270	14AUG2019:00:00:00	7	09:52	LRFI_fasted_R5
LRFI_fasted_R6	LRFI	fasted	FR17MAG201912987	2987	M	FR17MAG201812017	FR17MAG201811844	F	stomach	GJS201910	1230	14AUG2019:00:00:00	1	08:30	LRFI_fasted_R6
LRFI_fed_R1	LRFI	fed	FR17MAG201912885	2885	F	FR17MAG201811642	FR17MAG201811513	D	stomach	GJS201910	1260	14AUG2019:00:00:00	23	11:39	LRFI_fed_R1
LRFI_fed_R2	LRFI	fed	FR17MAG201912888	2888	M	FR17MAG201811642	FR17MAG201811513	D	stomach	GJS201910	1520	14AUG2019:00:00:00	22	11:30	LRFI_fed_R2
LRFI_fed_R3	LRFI	fed	FR17MAG201912898	2898	F	FR17MAG201811822	FR17MAG201812088	E	stomach	GJS201910	1420	14AUG2019:00:00:00	10	10:10	LRFI_fed_R3
LRFI_fed_R4	LRFI	fed	FR17MAG201912903	2903	M	FR17MAG201811822	FR17MAG201812088	E	stomach	GJS201910	1620	14AUG2019:00:00:00	20	11:17	LRFI_fed_R4
LRFI_fed_R5	LRFI	fed	FR17MAG201912981	2981	F	FR17MAG201812017	FR17MAG201811844	F	stomach	GJS201910	1210	14AUG2019:00:00:00	17	11:02	LRFI_fed_R5
LRFI_fed_R6	LRFI	fed	FR17MAG201912985	2985	M	FR17MAG201812017	FR17MAG201811844	F	stomach	GJS201910	1320	14AUG2019:00:00:00	19	11:14	LRFI_fed_R6
HRFI_fasted_R1	HRFI	fasted	FR17MAG201912990	2990	F	FR17MAG201811418	FR17MAG201811893	A	duodenum	GJS201910	1770	14AUG2019:00:00:00	3	09:18	G11+_fed_R1
HRFI_fasted_R2	HRFI	fasted	FR17MAG201912992	2992	M	FR17MAG201811418	FR17MAG201811893	A	duodenum	GJS201910	1870	14AUG2019:00:00:00	14	10:47	G11+_fed_R2
HRFI_fasted_R3	HRFI	fasted	FR17MAG201913004	3004	F	FR17MAG201811206	FR17MAG201811884	B	duodenum	GJS201910	1050	14AUG2019:00:00:00	13	10:30	G11+_fed_R3
HRFI_fasted_R4	HRFI	fasted	FR17MAG201913011	3011	M	FR17MAG201811206	FR17MAG201811884	B	duodenum	GJS201910	1280	14AUG2019:00:00:00	12	10:28	G11+_fed_R4
HRFI_fasted_R5	HRFI	fasted	FR17MAG201913042	3042	F	FR17MAG201811508	FR17MAG201811894	C	duodenum	GJS201910	1370	14AUG2019:00:00:00	2	09:10	G11+_fed_R5
HRFI_fasted_R6	HRFI	fasted	FR17MAG201913050	3050	M	FR17MAG201811508	FR17MAG201811894	C	duodenum	GJS201910	1200	14AUG2019:00:00:00	9	10:04	G11+_fed_R6
HRFI_fed_R1	HRFI	fed	FR17MAG201912989	2989	F	FR17MAG201811418	FR17MAG201811893	A	duodenum	GJS201910	1830	14AUG2019:00:00:00	16	10:57	G11+_fasted_R1
HRFI_fed_R2	HRFI	fed	FR17MAG201912991	2991	M	FR17MAG201811418	FR17MAG201811893	A	duodenum	GJS201910	1710	14AUG2019:00:00:00	18	11:07	G11+_fasted_R2
HRFI_fed_R3	HRFI	fed	FR17MAG201913012	3012	M	FR17MAG201811206	FR17MAG201811884	B	duodenum	GJS201910	1790	14AUG2019:00:00:00	11	10:26	G11+_fasted_R3
HRFI_fed_R4	HRFI	fed	FR17MAG201913014	3014	M	FR17MAG201811206	FR17MAG201811884	B	duodenum	GJS201910	1640	14AUG2019:00:00:00	24	11:46	G11+_fasted_R4
HRFI_fed_R5	HRFI	fed	FR17MAG201913039	3039	F	FR17MAG201811508	FR17MAG201811894	C	duodenum	GJS201910	1430	14AUG2019:00:00:00	21	11:25	G11+_fasted_R5
HRFI_fed_R6	HRFI	fed	FR17MAG201913049	3049	M	FR17MAG201811508	FR17MAG201811894	C	duodenum	GJS201910	1580	14AUG2019:00:00:00	15	10:52	G11+_fasted_R6
LRFI_fasted_R1	LRFI	fasted	FR17MAG201912884	2884	F	FR17MAG201811642	FR17MAG201811513	D	duodenum	GJS201910	1040	14AUG2019:00:00:00	6	09:43	G11-_fed_R1
LRFI_fasted_R2	LRFI	fasted	FR17MAG201912889	2889	M	FR17MAG201811642	FR17MAG201811513	D	duodenum	GJS201910	1450	14AUG2019:00:00:00	8	10:00	G11-_fed_R2
LRFI_fasted_R3	LRFI	fasted	FR17MAG201912899	2899	F	FR17MAG201811822	FR17MAG201812088	E	duodenum	GJS201910	1200	14AUG2019:00:00:00	5	09:34	G11-_fed_R3
LRFI_fasted_R4	LRFI	fasted	FR17MAG201912902	2902	M	FR17MAG201811822	FR17MAG201812088	E	duodenum	GJS201910	1530	14AUG2019:00:00:00	4	09:30	G11-_fed_R4
LRFI_fasted_R5	LRFI	fasted	FR17MAG201912980	2980	F	FR17MAG201812017	FR17MAG201811844	F	duodenum	GJS201910	1270	14AUG2019:00:00:00	7	09:52	G11-_fed_R5
LRFI_fasted_R6	LRFI	fasted	FR17MAG201912987	2987	M	FR17MAG201812017	FR17MAG201811844	F	duodenum	GJS201910	1230	14AUG2019:00:00:00	1	08:30	G11-_fed_R6
LRFI_fed_R1	LRFI	fed	FR17MAG201912885	2885	F	FR17MAG201811642	FR17MAG201811513	D	duodenum	GJS201910	1260	14AUG2019:00:00:00	23	11:39	G11-_fasted_R1
LRFI_fed_R2	LRFI	fed	FR17MAG201912888	2888	M	FR17MAG201811642	FR17MAG201811513	D	duodenum	GJS201910	1520	14AUG2019:00:00:00	22	11:30	G11-_fasted_R2
LRFI_fed_R3	LRFI	fed	FR17MAG201912898	2898	F	FR17MAG201811822	FR17MAG201812088	E	duodenum	GJS201910	1420	14AUG2019:00:00:00	10	10:10	G11-_fasted_R3
LRFI_fed_R4	LRFI	fed	FR17MAG201912903	2903	M	FR17MAG201811822	FR17MAG201812088	E	duodenum	GJS201910	1620	14AUG2019:00:00:00	20	11:17	G11-_fasted_R4
LRFI_fed_R5	LRFI	fed	FR17MAG201912981	2981	F	FR17MAG201812017	FR17MAG201811844	F	duodenum	GJS201910	1210	14AUG2019:00:00:00	17	11:02	G11-_fasted_R5
LRFI_fed_R6	LRFI	fed	FR17MAG201912985	2985	M	FR17MAG201812017	FR17MAG201811844	F	duodenum	GJS201910	1320	14AUG2019:00:00:00	19	11:14	G11-_fasted_R6
