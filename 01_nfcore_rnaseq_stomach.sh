#!/bin/bash
#SBATCH -J rspg_stmch_rna
#SBATCH -p unlimitq
#SBATCH --mem=6G
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH -o output.out
#SBATCH -e error.out

module purge
module load bioinfo/nfcore-Nextflow-v20.11.0-edge

nextflow run nf-core/rnaseq -r 3.0 -profile genotoul -resume \
	--input rosepigs_stomach_samples.csv \
	--fasta Sus_scrofa.Sscrofa11.1.dna.toplevel.fa \
	--gtf Sus_scrofa.Sscrofa11.1.102.gtf \
	--save_reference \
	--aligner star_salmon \
	--pseudo_aligner salmon
    
