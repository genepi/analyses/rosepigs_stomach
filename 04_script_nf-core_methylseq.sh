#!/bin/bash
#SBATCH -J methylseq
#SBATCH -e /work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs/run20230717.err
#SBATCH -o /work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs/run20230717.log
#SBATCH -p unlimitq
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --export=ALL
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G

module purge
module load bioinfo/NextflowWorkflows/nfcore-Nextflow-v22.12.0-edge
nextflow pull nf-core/methylseq -r 2.4.0

nextflow run nf-core/methylseq -profile genotoul \
	-r 2.4.0 \
	-c '/work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs/methylseq.genotoul.config' \
	-resume \
	--input '/work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs/sample_sheet.csv' \
	--cytosine_report \
	--outdir '/work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs' \
	--em_seq \
	--fasta '/work/genphyse/genepi/data/pig/emseq/project_a2p3_rosepigs/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa' \
	--max_memory 1100GB \
	--max_cpus 128
