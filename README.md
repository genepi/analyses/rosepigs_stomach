# ROSEpigs stomach sample analysis

## RNA-seq

RNA-seq samples were processed by nf-core/rnaseq.

## EM-seq

EM-seq samples were processed by nf-core/methylseq.
